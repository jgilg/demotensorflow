﻿namespace AppDemoTF
{
    using System;
    using Android.App;
    using Android.Widget;
    using Android.OS;
    using Android.Support.V7.App;
    using Android.Views;
    using Android.Graphics;
    using System.Threading.Tasks;
    using Android.Content;
    using Android.Provider;
    using Microsoft.Cognitive.CustomVision.Prediction.Models;
    using Android.Content.PM;
    using Android;
    using Android.Support.V4.App;
    using AppDemoTF.Service;

    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
	public class MainActivity : AppCompatActivity
	{
        private Button AnalizePhotoModel;
        private Button AnalizePhotoService;
        private Button SelectPhoto;
        private Button TakePhoto;
        private Button TrainModel;
        private TextView Result;
        private ProgressBar ProgressBar;
        private ImageView PhotoView;

        /// <summary>
        /// Image selected
        /// </summary>
        private Bitmap _photo;

        /// <summary>
        /// Result of image tag prediction
        /// </summary>
        private ImageTagPredictionModel _resultPredict;

        /// <summary>
        /// Image Classifier
        /// </summary>
        private ImageClassifier _imageClassifier;

        /// <summary>
        /// Id for select image result
        /// </summary>
        public static readonly int PickImageId = 1000;

        /// <summary>
        /// Id for take image result
        /// </summary>
        public static readonly int TakeImageId = 2000;
        public Android.Support.V7.Widget.Toolbar Toolbar { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.activity_main);

			//Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            Toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            if (Toolbar != null) SetSupportActionBar(Toolbar);
            //SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(false);


            _imageClassifier = new ImageClassifier();
            AnalizePhotoModel = FindViewById<Button>(Resource.Id.analize_photo_model);
            AnalizePhotoService = FindViewById<Button>(Resource.Id.analize_photo_service);
            SelectPhoto = FindViewById<Button>(Resource.Id.select_photo_button);
            TakePhoto = FindViewById<Button>(Resource.Id.take_photo_button);
            TrainModel = FindViewById<Button>(Resource.Id.train_model);
            PhotoView = FindViewById<ImageView>(Resource.Id.photo);
            Result = FindViewById<TextView>(Resource.Id.result_label);
            ProgressBar = FindViewById<ProgressBar>(Resource.Id.progressbar);

            TakePhoto.Click += OnClickTakePhoto;
            TrainModel.Click += OnClickTrainModel;
            AnalizePhotoModel.Click += OnClickAnalizePhotoModel;
            AnalizePhotoService.Click += OnClickAnalizePhotoService;
            SelectPhoto.Click += OnClickSelectPhoto;
        }

        /// <summary>
        /// Take Photo method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickTakePhoto(object sender, EventArgs e)
        {
            Result.Text = "";
            TrainModel.Visibility = ViewStates.Invisible;
            ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera }, 100);
            if (CheckSelfPermission(Manifest.Permission.Camera) == Permission.Granted)
            {
                Intent intent = new Intent(MediaStore.ActionImageCapture);
                StartActivityForResult(intent, TakeImageId);
            }
            else
            {
                Toast.MakeText(ApplicationContext, "Oops an error occurred", ToastLength.Long).Show();
                return;
            }
        }

        /// <summary>
        /// Select Photo method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickSelectPhoto(object sender, EventArgs e)
        {
            Result.Text = "";
            TrainModel.Visibility = ViewStates.Invisible;
            Intent = new Intent();
            Intent.SetType("image/*");
            Intent.SetAction(Intent.ActionGetContent);
            StartActivityForResult(Intent.CreateChooser(Intent, "Select Picture"), PickImageId);            
        }

        /// <summary>
        /// Activity Result for select or take photo
        /// </summary>
        /// <param name="requestCode"></param>
        /// <param name="resultCode"></param>
        /// <param name="data"></param>
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if ((requestCode == PickImageId) && (resultCode == Android.App.Result.Ok) && (data != null))
            {
                _photo = MediaStore.Images.Media.GetBitmap(this.ContentResolver, data.Data);
                PhotoView.SetImageBitmap(_photo);
            }
            if ((requestCode == TakeImageId) && (resultCode == Android.App.Result.Ok) && (data != null))
            {
                _photo = (Bitmap)data.Extras.Get("data");
                PhotoView.SetImageBitmap(_photo);
            }
        }

        /// <summary>
        /// Analize Photo by Model method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnClickAnalizePhotoModel(object sender, EventArgs e)
        {
            TrainModel.Visibility = ViewStates.Invisible;
            ProgressBar.Visibility = ViewStates.Visible;
            if (_photo == null)
            {
                Toast.MakeText(ApplicationContext, "First select or take a photo", ToastLength.Long).Show();
                ProgressBar.Visibility = ViewStates.Invisible;
                return; 
            }
            var result = await Task.Run(() => _imageClassifier.ImageLable(_photo));
            Result.Text = result;
            ProgressBar.Visibility = ViewStates.Invisible;
        }

        /// <summary>
        /// Analize Photo by Service method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnClickAnalizePhotoService(object sender, EventArgs e)
        {
            if (_photo == null)
            {
                Toast.MakeText(ApplicationContext, "First select or take a photo", ToastLength.Long).Show();
                ProgressBar.Visibility = ViewStates.Invisible;
                return;
            }
            ProgressBar.Visibility = ViewStates.Visible;
            _resultPredict = await Task.Run(() =>  CustomVisionService.Predict(_photo) );
            if (_resultPredict == null)
            {
                Toast.MakeText(ApplicationContext, "Oops an error occurred", ToastLength.Long).Show();
                ProgressBar.Visibility = ViewStates.Invisible;
                return;
            }
            Result.Text = $"{_resultPredict.Tag}: {_resultPredict.Probability:p1}";
            TrainModel.Visibility = ViewStates.Visible;
            ProgressBar.Visibility = ViewStates.Invisible;
        }

        /// <summary>
        /// Train Model method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnClickTrainModel(object sender, EventArgs e)
        {
            ProgressBar.Visibility = ViewStates.Visible;
            var result = await Task.Run(() => CustomVisionService.Train(_photo, _resultPredict.TagId.ToString()));
            if(result == null)
            {
                Toast.MakeText(ApplicationContext, "Oops an error occurred", ToastLength.Long).Show();
                return;
            }
            Result.Text = result;
            ProgressBar.Visibility = ViewStates.Invisible;
        }
    }
}

