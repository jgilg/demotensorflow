﻿namespace AppDemoTF.Helpers
{
    public class Constants
    {
        /// <summary>
        /// Predict Key of service
        /// </summary>
        public static readonly string PredictKey = "21a9a103405f4a039cbfad1dbefcd0d8";

        /// <summary>
        /// Traing key of service
        /// </summary>
        public static readonly string TrainingKey = "146f756c8cb847bcb1d296cc1239a55b";

        /// <summary>
        /// Proyect Id
        /// </summary>
        public static readonly string ProyectId = "a25e8e59-bf7a-4f51-bf13-ce5cef059ec9";
    }
}