﻿namespace AppDemoTF
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Android.App;
    using Android.Graphics;
    using Org.Tensorflow.Contrib.Android;

    /// <summary>
    /// Image classifier by model
    /// </summary>
    public class ImageClassifier
    {
        /// <summary>
        /// TensorFlow Inference Interface
        /// </summary>
        private TensorFlowInferenceInterface _inferenceInterface;

        /// <summary>
        /// Lables of model
        /// </summary>
        private List<string> Labels;

        /// <summary>
        /// Image Classifier constructor
        /// </summary>
        public ImageClassifier()
        {
            var assets = Application.Context.Assets;
            _inferenceInterface = new TensorFlowInferenceInterface(assets, "model.pb");
            var content = new StreamReader(assets.Open("labels.txt")).ReadToEnd();
            Labels = content.Split('\n').Select(s => s.Trim()).Where(s => !string.IsNullOrEmpty(s)).ToList();
        }

        /// <summary>
        /// Aanalize an image in the model
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public string ImageLable(Bitmap image)
        {
            //Convert the image to the correct format
            var resizedBitmap = Bitmap.CreateScaledBitmap(image, 227, 227, false)
                              .Copy(Bitmap.Config.Argb8888, false);
            var floatValues = new float[227 * 227 * 3];
            var intValues = new int[227 * 227];
            resizedBitmap.GetPixels(intValues, 0, resizedBitmap.Width, 0, 0, resizedBitmap.Width, resizedBitmap.Height);
            for (int i = 0; i < intValues.Length; ++i)
            {
                var val = intValues[i];
                floatValues[i * 3 + 0] = ((val & 0xFF) - 104);
                floatValues[i * 3 + 1] = (((val >> 8) & 0xFF) - 117);
                floatValues[i * 3 + 2] = (((val >> 16) & 0xFF) - 123);
            }

            //Run model
            var outputs = new float[Labels.Count];
            _inferenceInterface.Feed("Placeholder", floatValues, 1, 227, 227, 3);
            _inferenceInterface.Run(new[] { "loss" });
            _inferenceInterface.Fetch("loss", outputs);

            //View results
            var results = new List<Tuple<float, string>>();
            for (var i = 0; i < outputs.Length; ++i)
                results.Add(Tuple.Create(outputs[i], Labels[i]));

            return $"{results.OrderByDescending(t => t.Item1).First().Item2} - {results.OrderByDescending(t => t.Item1).First().Item1:p1}";
        }
    }
}