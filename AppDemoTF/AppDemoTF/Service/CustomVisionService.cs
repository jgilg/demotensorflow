﻿namespace AppDemoTF.Service
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using Android.Graphics;
    using AppDemoTF.Helpers;
    using Microsoft.Cognitive.CustomVision.Prediction;
    using Microsoft.Cognitive.CustomVision.Prediction.Models;
    using Microsoft.Cognitive.CustomVision.Training;

    /// <summary>
    /// Custom Vision Service
    /// </summary>
    public static class CustomVisionService
    {
        /// <summary>
        /// Analize an image consulting the service
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static ImageTagPredictionModel Predict(Bitmap image)
        {
            PredictionEndpoint predicting = new PredictionEndpoint() { ApiKey = Constants.PredictKey };
            ImageTagPredictionModel resultPredict;
            try
            {
                resultPredict = predicting.PredictImage(new Guid(Constants.ProyectId), GetBitmapStream(image)).Predictions.First();
            }
            catch (Exception)
            {
                return null;
            }
            return resultPredict;
        }

        /// <summary>
        /// Train an image method
        /// </summary>
        /// <param name="image"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public static string Train(Bitmap image, string tagId)
        {
            TrainingApi trainingApi = new TrainingApi() { ApiKey = Constants.TrainingKey };
            var projectId = new Guid(Constants.ProyectId);
            try
            {
                trainingApi.CreateImagesFromData(projectId, GetBitmapStream(image), new List<string>() { tagId });            
                var iteration = trainingApi.TrainProject(projectId);
                while (!iteration.Status.Equals("Completed"))
                {
                    Thread.Sleep(1000);
                    iteration = trainingApi.GetIteration(projectId, iteration.Id);
                }
                iteration.IsDefault = true;
                trainingApi.UpdateIteration(projectId, iteration.Id, iteration);
                return "Modelo Entrenado";
            }
            catch (Exception)
            {
                return "Oops an error occurred";
            }
        }

        /// <summary>
        /// Get stream of bitmap method
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static MemoryStream GetBitmapStream(Bitmap image)
        {
            byte[] bitmapData;
            using (var stream = new MemoryStream())
            {
                image.Compress(Bitmap.CompressFormat.Png, 0, stream);
                bitmapData = stream.ToArray();
            }
            return new MemoryStream(bitmapData);
        }
    }
}